package br.com.plg.xteam.daw.purchases.model.services;

import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import br.com.plg.xteam.daw.purchases.model.internal.UserDto;
import br.com.plg.xteam.daw.purchases.util.IOUtil;

@RunWith(SpringRunner.class)
@RestClientTest(UserApiService.class)
@ComponentScan(basePackages = "br.com.plg.xteam.daw.util")
public class UserApiServiceTest {

	@Autowired
	private UserApiService userApiService;

	@Autowired
	private MockRestServiceServer server;

	@Test
	public void testFound() throws IOException {

		InputStream st = this.getClass().getResourceAsStream("/user.json");
		String jsonString = IOUtil.read(st);

		server.expect(once(), requestTo("/Darby.Mraz82"))
				.andExpect(method(HttpMethod.GET)).andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));

		UserDto user = userApiService.getUser("Darby.Mraz82");
		Assert.assertEquals("Darby.Mraz82", user.getUsername());
		Assert.assertEquals("Darby.Mraz82@gmail.com", user.getEmail());

		// assert cache was hit, since only one expectation to the server was
		// made
		user = userApiService.getUser("Darby.Mraz82");
		Assert.assertEquals("Darby.Mraz82", user.getUsername());
		Assert.assertEquals("Darby.Mraz82@gmail.com", user.getEmail());

	}

	@Test
	public void testNotFound() throws IOException {


		server.expect(once(), requestTo("/notfound")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("{}", MediaType.APPLICATION_JSON));

		UserDto user = userApiService.getUser("notfound");
		Assert.assertNull(user);

		// assert cache was hit, since only one expectation to the server was
		// made
		user = userApiService.getUser("notfound");
		Assert.assertNull(user);

	}

}
