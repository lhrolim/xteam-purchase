package br.com.plg.xteam.daw.purchases.model.services;

import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import br.com.plg.xteam.daw.purchases.model.internal.PurchaseDto;
import br.com.plg.xteam.daw.purchases.util.IOUtil;

@RunWith(SpringRunner.class)
@RestClientTest(PurchaseApiService.class)
@ComponentScan(basePackages = "br.com.plg.xteam.daw.util")
public class PurchaseApiServiceTest {

	@Autowired
	private PurchaseApiService purchaseApiService;

	@Autowired
	private MockRestServiceServer server;

	@Test
	public void testFoundByUser() throws IOException {

		InputStream st = this.getClass().getResourceAsStream("/purchases_byuser.json");
		String jsonString = IOUtil.read(st);

		server.expect(once(), requestTo("/by_user/Darby.Mraz82?limit=5")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));

		List<PurchaseDto> purchases = purchaseApiService.getPurchasesByUser("Darby.Mraz82", 5);
		Assert.assertEquals(5, purchases.size());

	}

	@Test
	public void testNotFoundByUser() throws IOException {

		server.expect(once(), requestTo("/by_user/notfound?limit=5")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("{}", MediaType.APPLICATION_JSON));

		List<PurchaseDto> purchases = purchaseApiService.getPurchasesByUser("notfound", 5);
		Assert.assertNotNull(purchases);
		Assert.assertEquals(0, purchases.size());

	}

	@Test
	public void testFoundByProduct() throws IOException {

		InputStream st = this.getClass().getResourceAsStream("/purchases_byproduct.json");
		String jsonString = IOUtil.read(st);

		server.expect(once(), requestTo("/by_product/15269"))
				.andExpect(method(HttpMethod.GET)).andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));

		Set<String> purchases = purchaseApiService.getPurchasesByProduct(15269, "Chase.Jast0");
		// excluding itself from the list
		Assert.assertEquals(2, purchases.size());
		Assert.assertTrue(purchases
				.containsAll(
						new HashSet<String>(Arrays.asList("Darby.Mraz82", "Loraine.Balistreri"))));


	}

	@Test
	public void testNotFound() throws IOException {


		server.expect(once(), requestTo("/by_product/1000")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("{}", MediaType.APPLICATION_JSON));

		Set<String> products = purchaseApiService.getPurchasesByProduct(1000, "test");
		Assert.assertNotNull(products);
		Assert.assertEquals(0, products.size());


	}

}
