package br.com.plg.xteam.daw.purchases.model.services;

import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import br.com.plg.xteam.daw.purchases.model.internal.ProductDto;
import br.com.plg.xteam.daw.purchases.util.IOUtil;

@RunWith(SpringRunner.class)
@RestClientTest(ProductApiService.class)
@ComponentScan(basePackages = "br.com.plg.xteam.daw.util")
public class ProductApiServiceTest {

	@Autowired
	private ProductApiService productService;

	@Autowired
	private MockRestServiceServer server;

	@Test
	public void testFound() throws IOException {

		InputStream st = this.getClass().getResourceAsStream("/product_data.json");
		String jsonString = IOUtil.read(st);

		server.expect(once(), requestTo("/15269"))
				.andExpect(method(HttpMethod.GET)).andRespond(withSuccess(jsonString, MediaType.APPLICATION_JSON));

		ProductDto productDto = productService.getProductData(15269);
		Assert.assertEquals("15269", productDto.getId());
		Assert.assertEquals("939", productDto.getPrice());

		// assert cache was hit, since only one server expectation was made
		productDto = productService.getProductData(15269);
		Assert.assertEquals("15269", productDto.getId());
		Assert.assertEquals("939", productDto.getPrice());

	}

	@Test
	public void testNotFound() throws IOException {
		server.expect(once(), requestTo("/155")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("{}", MediaType.APPLICATION_JSON));

		ProductDto productDto = productService.getProductData(155);
		Assert.assertNull(productDto);

		// assert cache was hit, since only one server expectation was made
		productDto = productService.getProductData(155);
		Assert.assertNull(productDto);

	}

}
