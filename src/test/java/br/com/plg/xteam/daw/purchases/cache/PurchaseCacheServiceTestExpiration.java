package br.com.plg.xteam.daw.purchases.cache;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import br.com.plg.xteam.daw.purchases.model.PopularPurchase;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "cache.ttlinmillis=2000")
@ComponentScan(basePackages = "br.com.plg.xteam.daw.util")
public class PurchaseCacheServiceTestExpiration {

	@Autowired
	private PurchaseCacheService service;
	

	@Test
	public void testCacheExpiration() throws InterruptedException {
		List<PopularPurchase> firstCall = service.getPopularPurchases("testtoexpire");
		Assert.isNull(firstCall);
		List<PopularPurchase> list = new ArrayList<PopularPurchase>();
		list.add(new PopularPurchase());
		service.updatePopularPurchase("testtoexpire", list);

		// testing cache expiration
		Thread.sleep(2000);

		Assert.isNull(service.getPopularPurchases("test"));

	}

}

