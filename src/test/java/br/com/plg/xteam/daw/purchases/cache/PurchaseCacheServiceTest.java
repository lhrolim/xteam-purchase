package br.com.plg.xteam.daw.purchases.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import br.com.plg.xteam.daw.purchases.model.PopularPurchase;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = "br.com.plg.xteam.daw.util")
public class PurchaseCacheServiceTest {

	@Autowired
	private PurchaseCacheService service;
	
	@Test
	public void test() {
		List<PopularPurchase> firstCall = service.getPopularPurchases("test");
		Assert.isNull(firstCall);
	}

	@Test
	public void testCacheHit() {
		List<PopularPurchase> firstCall = service.getPopularPurchases("test");
		Assert.isNull(firstCall);
		List<PopularPurchase> list = new ArrayList<PopularPurchase>();
		list.add(new PopularPurchase());
		service.updatePopularPurchase("test", list);

		List<PopularPurchase> cacheFilled = service.getPopularPurchases("test");
		org.junit.Assert.assertEquals(cacheFilled, list);
	}


	@Test
	public void testCacheNotFound() {
		service.updatePopularPurchase("test", null);

		List<PopularPurchase> cacheFilled = service.getPopularPurchases("test");
		org.junit.Assert.assertEquals(Collections.emptyList(), cacheFilled);
	}

}

