package br.com.plg.xteam.daw.purchases.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import br.com.plg.xteam.daw.purchases.cache.PurchaseCacheService;
import br.com.plg.xteam.daw.purchases.model.PopularPurchase;
import br.com.plg.xteam.daw.purchases.model.internal.ProductDto;
import br.com.plg.xteam.daw.purchases.model.internal.PurchaseDto;
import br.com.plg.xteam.daw.purchases.model.internal.UserDto;
import br.com.plg.xteam.daw.purchases.model.services.ProductApiService;
import br.com.plg.xteam.daw.purchases.model.services.PurchaseApiService;
import br.com.plg.xteam.daw.purchases.model.services.UserApiService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = "br.com.plg.xteam.daw.util")
public class PopularPurchaseControllerTest {

	@Autowired
	private PopularPurchaseController controller;

	// Real services are already tested individually
	private ProductApiService mockedProductApi = Mockito.mock(ProductApiService.class);
	private PurchaseApiService mockedPurchaseApi = Mockito.mock(PurchaseApiService.class);
	private UserApiService mockedUserApi = Mockito.mock(UserApiService.class);
	private PurchaseCacheService cacheService = Mockito.mock(PurchaseCacheService.class);

	@Before
	public void init() {
		Mockito.reset(mockedProductApi, mockedPurchaseApi, mockedUserApi, cacheService);
		ReflectionTestUtils.setField(controller, "productApiService", mockedProductApi);
		ReflectionTestUtils.setField(controller, "purchaseApiService", mockedPurchaseApi);
		ReflectionTestUtils.setField(controller, "userApiService", mockedUserApi);
		ReflectionTestUtils.setField(controller, "cacheService", cacheService);
	}


	private static final String username = "USER";

	@Test
	public void testUserNotFound() {
		when(cacheService.getPopularPurchases("test")).thenReturn(null);
		when(mockedUserApi.getUser("test")).thenReturn(null);
		ResponseEntity<?> response = controller.RecentPurchase("test");
		Assert.assertEquals(HttpStatus.NOT_FOUND.name(), response.getStatusCode().name());
		Assert.assertEquals(String.format(PopularPurchaseController.NotFoundPattern, "test"),
				response.getBody());

		Mockito.verifyZeroInteractions(mockedProductApi, mockedPurchaseApi);
		Mockito.verify(cacheService).getPopularPurchases("test");
		Mockito.verify(mockedUserApi).getUser("test");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUserFound() {

		when(cacheService.getPopularPurchases(username)).thenReturn(null);
		when(mockedUserApi.getUser(username)).thenReturn(new UserDto(username, username));
		
		List<PurchaseDto> list = new ArrayList<PurchaseDto>();
		list.add(new PurchaseDto(1, username, Calendar.getInstance().getTime(), 1));
		list.add(new PurchaseDto(2, username, Calendar.getInstance().getTime(), 2));
		list.add(new PurchaseDto(3, username, Calendar.getInstance().getTime(), 3));

		when(mockedPurchaseApi.getPurchasesByUser(username, 5)).thenReturn(list);
		
		// each of the products
		when(mockedPurchaseApi.getPurchasesByProduct(1, username))
				.thenReturn(new HashSet<String>(Arrays.asList("user1", "user2")));
		// product 2 is the most popular--> will assert sort later
		when(mockedPurchaseApi.getPurchasesByProduct(2, username))
				.thenReturn(new HashSet<String>(Arrays.asList("user1", "user2", "user4", "user5")));
		when(mockedPurchaseApi.getPurchasesByProduct(3, username))
				.thenReturn(new HashSet<String>(Arrays.asList("user1", "user5", "user6")));

		// each of the products
		when(mockedProductApi.getProductData(1)).thenReturn(new ProductDto("1", "face1", "price1", "size1"));
		when(mockedProductApi.getProductData(2)).thenReturn(new ProductDto("2", "face1", "price1", "size1"));
		when(mockedProductApi.getProductData(3)).thenReturn(new ProductDto("3", "face1", "price1", "size1"));

		ResponseEntity<List<PopularPurchase>> response = (ResponseEntity<List<PopularPurchase>>) controller
				.RecentPurchase(username);
		Assert.assertEquals(HttpStatus.OK.name(), response.getStatusCode().name());
		List<PopularPurchase> listResult = response.getBody();
		Assert.assertEquals(3, listResult.size());
		PopularPurchase firstItem = listResult.get(0);
		Assert.assertEquals("2", firstItem.getProductData().getId());
		Assert.assertEquals(Integer.valueOf(4), firstItem.PurchaseRank());

		PopularPurchase lastItem = listResult.get(2);
		Assert.assertEquals("1", lastItem.getProductData().getId());
		Assert.assertEquals(Integer.valueOf(2), lastItem.PurchaseRank());
		Assert.assertEquals(Integer.valueOf(2), Integer.valueOf(lastItem.getRecent().size()));
		
		Mockito.verify(cacheService).getPopularPurchases(username);
		Mockito.verify(cacheService).updatePopularPurchase(username, listResult);
		Mockito.verify(mockedUserApi).getUser(username);
		Mockito.verify(mockedPurchaseApi).getPurchasesByUser(username, 5);
		Mockito.verify(mockedPurchaseApi).getPurchasesByProduct(1, username);
		Mockito.verify(mockedPurchaseApi).getPurchasesByProduct(2, username);
		Mockito.verify(mockedPurchaseApi).getPurchasesByProduct(3, username);

		Mockito.verify(mockedProductApi).getProductData(1);
		Mockito.verify(mockedProductApi).getProductData(2);
		Mockito.verify(mockedProductApi).getProductData(3);


	}
	

	@SuppressWarnings("unchecked")
	@Test
	public void testCache() {

		List<PopularPurchase> list = new ArrayList<PopularPurchase>();
		list.add(new PopularPurchase());
		list.add(new PopularPurchase());
		when(cacheService.getPopularPurchases(username)).thenReturn(list);

		ResponseEntity<List<PopularPurchase>> response = (ResponseEntity<List<PopularPurchase>>) controller
				.RecentPurchase(username);

		Assert.assertEquals(HttpStatus.OK.name(), response.getStatusCode().name());

		List<PopularPurchase> listResult = response.getBody();
		Assert.assertEquals(listResult, list);

		Mockito.verify(cacheService).getPopularPurchases(username);

		Mockito.verifyZeroInteractions(mockedProductApi, mockedPurchaseApi, mockedUserApi);

	}

}
