package br.com.plg.xteam.daw.purchases.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.plg.xteam.daw.purchases.cache.PurchaseCacheService;
import br.com.plg.xteam.daw.purchases.model.PopularPurchase;
import br.com.plg.xteam.daw.purchases.model.internal.ProductDto;
import br.com.plg.xteam.daw.purchases.model.internal.PurchaseDto;
import br.com.plg.xteam.daw.purchases.model.internal.UserDto;
import br.com.plg.xteam.daw.purchases.model.services.ProductApiService;
import br.com.plg.xteam.daw.purchases.model.services.PurchaseApiService;
import br.com.plg.xteam.daw.purchases.model.services.UserApiService;

@RestController
public class PopularPurchaseController {

	static final String NotFoundPattern = "User with username of '%s' was not found";

	private ExecutorService executor;

	@Autowired
	private ProductApiService productApiService;

	@Autowired
	private PurchaseApiService purchaseApiService;

	@Autowired
	private UserApiService userApiService;

	@Autowired
	private PurchaseCacheService cacheService;


	private int recentLimit;

	private static final Logger log = LoggerFactory.getLogger(PopularPurchaseController.class);

	public PopularPurchaseController(ProductApiService productApiService, PurchaseApiService purcahseApiService,
			UserApiService userApiService, PurchaseCacheService cacheService,
			@Value("${recentlimit}") int recentLimit) {
		// implementation took from Executors.newCachedThreadPool(), limiting
		// the max number of concurrent threads,
		// however, to prevent any memory leak for highly loaded servers
		// allowing up to 200 threads to live simultaneously
		// keeping a minimum of 10 threads on the pool, by default, 5 for
		// product fetching and another 5 for purchase fetching
		executor = new ThreadPoolExecutor(10, 200, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
		this.productApiService = productApiService;
		this.purchaseApiService = purcahseApiService;
		this.userApiService = userApiService;
		this.cacheService = cacheService;
		this.recentLimit = recentLimit;
	}

	/**
	 * Brings a list of recently purchased products, and the names of other
	 * users who recently purchased them, using the following algorithm as a
	 * template:
	 * 
	 * 
	 * <ul>
	 * 
	 * <li>fetch 5 recent purchases for the user: GET
	 * /api/purchases/by_user/:username?limit=5</li>
	 * <li>for each of those products, get a list of all people who previously
	 * purchased that product: GET /api/purchases/by_product/:product_id</li>
	 * <li>at the same time, request info about the products: GET
	 * /api/products/:product_id</li>
	 * <li>finally, put all of the data together and sort it so that the product
	 * with the highest number of recent purchases is first</li>
	 *
	 * </ul>
	 * 
	 * Up to 10 threads are allocated to the task so that the fetchs can be
	 * fully parallelized
	 * 
	 * @param username
	 * @return a json representing the result
	 */
	@RequestMapping(value = "/api/recent_purchases/{username:.+}", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> RecentPurchase(@PathVariable(value = "username") String username) {

		List<PopularPurchase> cachedResults = cacheService.getPopularPurchases(username);

		if (cachedResults != null) {
			return new ResponseEntity<List<PopularPurchase>>(cachedResults, HttpStatus.OK);
		}

		UserDto user = userApiService.getUser(username);
		if (user == null) {
			cacheService.updatePopularPurchase(username, null);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(String.format(NotFoundPattern, username));
		}

		List<PurchaseDto> purchases = purchaseApiService.getPurchasesByUser(username, recentLimit);

		List<PopularPurchase> popularPurchases = new ArrayList<PopularPurchase>();

		for (PurchaseDto purchaseDto : purchases) {
			int productId = purchaseDto.getProductId();
			try {
				addPopularPurchase(popularPurchases, productId, username);
			} catch (InterruptedException | ExecutionException e) {
				log.error(e.getMessage(), e);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.body("Could not process the request now, please contact support");
			}
		}
		// sorting by number of purchases
		popularPurchases.sort(new PopularPurchase.PopularPurchaseComparator());

		cacheService.updatePopularPurchase(username, popularPurchases);

		return new ResponseEntity<List<PopularPurchase>>(popularPurchases, HttpStatus.OK);

	}

	/**
	 * For a given product fetches its purchases and data on separate threads,
	 * grouping the results back on the popularPurchases parameter.
	 * 
	 * 
	 * 
	 * 
	 * @param popularPurchases
	 * @param productId
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private void addPopularPurchase(List<PopularPurchase> popularPurchases, int productId, String username)
			throws InterruptedException, ExecutionException {
		PopularPurchase popularPurchase = new PopularPurchase();
		popularPurchases.add(popularPurchase);

		Future<ProductDto> productDTO = executor.submit(() -> {
			return productApiService.getProductData(productId);
		});

		Future<Set<String>> purchasesOfProduct = executor.submit(() -> {
			return purchaseApiService.getPurchasesByProduct(productId, username);
		});

		popularPurchase.setProductData(productDTO.get());
		popularPurchase.setRecent(purchasesOfProduct.get());

	}

}
