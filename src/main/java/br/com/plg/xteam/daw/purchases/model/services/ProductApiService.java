package br.com.plg.xteam.daw.purchases.model.services;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.plg.xteam.daw.purchases.model.internal.ProductDTOWrapper;
import br.com.plg.xteam.daw.purchases.model.internal.ProductDto;

@Component
public class ProductApiService {


	/**
	 * Assuming product data won�t change, since there�s no method on the api to
	 * allow so
	 */
	private Map<Integer, ProductDTOWrapper> cache = new ConcurrentHashMap<Integer, ProductDTOWrapper>();

	private RestTemplate restTemplate;

	private static final Logger log = LoggerFactory.getLogger(ProductApiService.class);

	public ProductApiService(RestTemplateBuilder restTemplateBuilder, @Value("${api.baseurl}") String rootURL) {
		restTemplate = restTemplateBuilder.rootUri(rootURL + "products").build();
	}

	public ProductDto getProductData(int productId) {
		if (cache.containsKey(productId)) {
			log.info("returning productid {} from cache", productId);
			return cache.get(productId).getProduct();
		}
		ProductDTOWrapper productDto = restTemplate.getForObject("/{productId}", ProductDTOWrapper.class, productId);
		log.info("retrieved productid from remote server", productId);
		cache.put(productId, productDto);
		return productDto.getProduct();

	}

}
