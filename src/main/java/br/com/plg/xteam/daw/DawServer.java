package br.com.plg.xteam.daw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DawServer {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(DawServer.class);
		application.run(args);
	}

}
