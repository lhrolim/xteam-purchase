package br.com.plg.xteam.daw.purchases.model.internal;

import java.util.Comparator;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseDto {

	private int id;
	private String username;
	private Date date;
	private int productId;

	public PurchaseDto() {
		// TODO Auto-generated constructor stub
	}

	public PurchaseDto(int id, String username, Date date, int productId) {
		super();
		this.id = id;
		this.username = username;
		this.date = date;
		this.productId = productId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	public static class PurchaseDTOComparator implements Comparator<PurchaseDto>{

		@Override
		public int compare(PurchaseDto o1, PurchaseDto o2) {
			if (o1.getUsername().equals(o2.getUsername())){
				return 0;
			}
			//keeping most recent entries at beginning
			return o2.getDate().compareTo(o1.getDate());
		}
		
	}
	
	

}
