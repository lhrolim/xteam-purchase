package br.com.plg.xteam.daw.util;

import java.io.IOException;
import java.lang.annotation.Annotation;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;

@Component
public class CustomObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper {

	private static final long serialVersionUID = -6965073328505997069L;

	private void autoconfigureFeatures(JavaType javaType) {
		Annotation rootAnnotation = javaType.getRawClass().getAnnotation(JsonRootName.class);
		this.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, rootAnnotation != null);
		this.configure(SerializationFeature.INDENT_OUTPUT, true);
	}

	@Override
	protected Object _readMapAndClose(JsonParser jsonParser, JavaType javaType)
			throws IOException, JsonParseException, JsonMappingException {
		autoconfigureFeatures(javaType);
		return super._readMapAndClose(jsonParser, javaType);
	}

}
