package br.com.plg.xteam.daw.purchases.model.internal;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseCollectionWrapper {

	private List<PurchaseDto> purchases = new ArrayList<PurchaseDto>();

	@JsonProperty("purchases")
	public List<PurchaseDto> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<PurchaseDto> purchases) {
		this.purchases = purchases;
	}

}
