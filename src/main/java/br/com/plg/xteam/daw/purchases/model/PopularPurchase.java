package br.com.plg.xteam.daw.purchases.model;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import br.com.plg.xteam.daw.purchases.model.internal.ProductDto;

/**
 * @author luizh
 *
 */
public class PopularPurchase {

	@JsonUnwrapped
	private ProductDto productData;
	private Set<String> recent = new HashSet<String>();

	public ProductDto getProductData() {
		return productData;
	}

	public Set<String> getRecent() {
		return recent;
	}

	public void setRecent(Set<String> recent) {
		this.recent = recent;
	}

	public void setProductData(ProductDto productData) {
		this.productData = productData;
	}

	/**
	 * @return the number of purchases that have been made recently for this
	 *         product
	 */
	public Integer PurchaseRank() {
		return recent.size();
	}


	@Override
	public String toString() {
		return "product: " + this.productData.getId() + " recent count:" + PurchaseRank();
	}

	public static class PopularPurchaseComparator implements Comparator<PopularPurchase> {

		@Override
		public int compare(PopularPurchase o1, PopularPurchase o2) {
			// reversing the order so that the highest rank stays first
			return o2.PurchaseRank().compareTo(o1.PurchaseRank());
		}

	}

}
