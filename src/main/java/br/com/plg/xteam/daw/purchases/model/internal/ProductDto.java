package br.com.plg.xteam.daw.purchases.model.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDto {

	private String id;
	private String face;
	private String price;
	private String size;

	public ProductDto() {
		// TODO Auto-generated constructor stub
	}

	public ProductDto(String id, String face, String price, String size) {
		super();
		this.id = id;
		this.face = face;
		this.price = price;
		this.size = size;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

}
