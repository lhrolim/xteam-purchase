package br.com.plg.xteam.daw.purchases.model.services;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.plg.xteam.daw.purchases.model.internal.PurchaseCollectionWrapper;
import br.com.plg.xteam.daw.purchases.model.internal.PurchaseDto;

@Component
public class PurchaseApiService  {

	

	private static final Logger log = LoggerFactory.getLogger(PurchaseApiService.class);
	

	private RestTemplate restTemplate;
	
	public PurchaseApiService(RestTemplateBuilder restTemplateBuilder, @Value("${api.baseurl}") String rootURL) {
		restTemplate = restTemplateBuilder.rootUri(rootURL + "purchases").build();
	}
	

	public List<PurchaseDto> getPurchasesByUser(String username,int limit) {
		log.debug("fetching latest {} purchases for user {}", limit, username);
		PurchaseCollectionWrapper response = restTemplate.getForObject(
				"/by_user/{username}?limit={limit}", PurchaseCollectionWrapper.class, username,
				limit);
		return response.getPurchases();
	}

	
	public Set<String> getPurchasesByProduct(int productId, String username) {
		log.debug("fetching latest purchases for user product {}", productId);
		PurchaseCollectionWrapper response = restTemplate.getForObject("/by_product/{productId}",
				PurchaseCollectionWrapper.class, productId);
		TreeSet<PurchaseDto> result = new TreeSet<PurchaseDto>(new PurchaseDto.PurchaseDTOComparator());
		result.addAll(response.getPurchases());
		return result.stream().map(a -> a.getUsername()).filter(f -> !f.equalsIgnoreCase(username))
				.collect(Collectors.toSet());
	}


}
