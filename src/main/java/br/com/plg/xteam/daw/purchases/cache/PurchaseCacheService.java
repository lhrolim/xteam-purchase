package br.com.plg.xteam.daw.purchases.cache;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import br.com.plg.xteam.daw.purchases.model.PopularPurchase;

/**
 * Service to store caches for purchases, which are subject of changing.
 * 
 * Other endpoints, such as product and user, are assumed static, since there
 * were no methods on the api for updates
 * 
 * @author luizh
 *
 */
@Component
public class PurchaseCacheService {

	private Logger log = org.slf4j.LoggerFactory.getLogger(PurchaseCacheService.class);


	/*
	 * Using guava to store a cache with a TTL of 10 minutes
	 */
	private ConcurrentMap<String, PopularPurchaseWrapper> popularPurchaseMap;

	public PurchaseCacheService(@Value("${cache.ttlinmillis}") int ttl) {
		Cache<String, PopularPurchaseWrapper> cache = CacheBuilder.newBuilder()
				.expireAfterWrite(ttl, TimeUnit.MILLISECONDS).build();
		popularPurchaseMap = cache.asMap();
	}

	public List<PopularPurchase> getPopularPurchases(String username) {
		if (popularPurchaseMap.containsKey(username)) {
			log.info("returning cached result for {}", username);
			PopularPurchaseWrapper wrapper = popularPurchaseMap.get(username);
			if (wrapper.purchases == null) {
				return Collections.emptyList();
			}
			return wrapper.purchases;
		}
		return null;
	}

	public void updatePopularPurchase(String username, List<PopularPurchase> purchases) {
		popularPurchaseMap.put(username, new PopularPurchaseWrapper(purchases));
	}

	class PopularPurchaseWrapper {

		public PopularPurchaseWrapper(List<PopularPurchase> purchases) {
			this.purchases = purchases;
		}

		List<PopularPurchase> purchases;

		public List<PopularPurchase> getPurchases() {
			return purchases;
		}

		public void setPurchases(List<PopularPurchase> purchases) {
			this.purchases = purchases;
		}

	}

}
