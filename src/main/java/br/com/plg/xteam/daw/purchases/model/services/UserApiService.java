package br.com.plg.xteam.daw.purchases.model.services;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.plg.xteam.daw.purchases.model.internal.UserDto;
import br.com.plg.xteam.daw.purchases.model.internal.UserWrapper;

@Component
public class UserApiService {

	private static final Logger log = LoggerFactory.getLogger(UserApiService.class);

	private Map<String, UserDto> cache = new ConcurrentHashMap<String, UserDto>();

	private RestTemplate restTemplate;

	public UserApiService(RestTemplateBuilder restTemplateBuilder, @Value("${api.baseurl}") String rootURL) {
		restTemplate = restTemplateBuilder.rootUri(rootURL + "users").build();
	}

	public UserDto getUser(String username) {
		if (cache.containsKey(username)) {
			UserDto user = cache.get(username);
			log.info("returning cached user for {}", username);
			if (user instanceof EmptyUser) {
				// concurrent hashmaps do not allow null keys or values
				// http://stackoverflow.com/questions/698638/why-does-concurrenthashmap-prevent-null-keys-and-values
				return null;
			}
			return user;
		}

		log.info("fetching user data for {}", username);

		UserWrapper userWrapper = restTemplate.getForObject("/{username}", UserWrapper.class, username);
		if (userWrapper == null || userWrapper.getUser() == null) {
			cache.put(username, new EmptyUser());
			return null;
		}
		log.info("user {} found. Adding to cache", username);
		cache.put(username, userWrapper.getUser());
		return userWrapper.getUser();

	}

	// concurrent hashmaps do not allow null keys or values
	// http://stackoverflow.com/questions/698638/why-does-concurrenthashmap-prevent-null-keys-and-values
	private class EmptyUser extends UserDto {

	}


}
