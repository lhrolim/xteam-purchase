## xteam-purchase
xteam-purchase

The exam was implemented using spring-boot and maven capabilities.

In order to reduce the size of zip, no external dependencies were provided, but a simple mvn clean install would be enough to have it executing.

I am also attaching an executable file called xteam.daw-0.1.0.jar. This file can be executed, seamlessly, using the command:

*java-jar xteam.daw-0.1.0.jar* (which is equivalent of running mvn spring-boot:run). 
Both would start the server at port 8080

Useful Maven Commands:

mvn eclipse:eclipse --> generates eclipse .project, .classpath and other settings file (or, a maven plugin could also be used)
mvn clean install --> generates an executable jar file, running all the tests of the project
mvn spring-boot:run --> Starts the server, using an embedded tomcat
mvn spring-boot:run@debug --> Starts the server, waiting on a remote debugger at port 5004
mvn cobertura:cobertura --> Generates test-run coverage, at target\site\cobertura
mvn clean package --> generates the executable file at the target folder

##Properties 

The following properties can be overriden, via command the line:

cache.ttlinmillis=60000  
api.baseurl=http://74.50.59.155:6000/api/  
recentlimit=5  
logging.file=xteam-daw.log  

ex: java-jar xteam.daw-0.1.0.jar --recentlimit=10

##Logging

All the logs are output both to the console and to a file called *xteam-daw.log* (which can be also overriden)







